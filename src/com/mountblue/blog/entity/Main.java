package com.mountblue.blog.entity;

import java.time.LocalDateTime;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {

	public static void main(String[] args) 
	{
		
		/*
		 * Post post1=new
		 * Post("hhfffffHow to dance","Shhhffffffhummary is to watch videos"
		 * ,"hfffffhfhhhow to dance content","hhffffhhbhanu",LocalDateTime.now(),false,
		 * LocalDateTime.now(),LocalDateTime.now()); Post post2=new
		 * Post("How to eatttttt","Summary is to exercise more","how to eat content"
		 * ,"laxmi",LocalDateTime.now(),true,LocalDateTime.now(),LocalDateTime.now());
		 * Comment c1= new
		 * Comment("fffhhpadha","ffffpd@foo.com","ffffgood explaination",LocalDateTime.
		 * now(),LocalDateTime.now()); Comment c2= new
		 * Comment("fffhhradha","ffffrd@foo.com","fffbad explaination",LocalDateTime.now
		 * (),LocalDateTime.now()); Comment c3= new
		 * Comment("ffffhhmadha","ffffmd@foo.com","fffok explaination",LocalDateTime.now
		 * (),LocalDateTime.now()); Tag t1=new
		 * Tag("maths",LocalDateTime.now(),LocalDateTime.now()); Tag t2=new
		 * Tag("space",LocalDateTime.now(),LocalDateTime.now()); Tag t3=new
		 * Tag("games",LocalDateTime.now(),LocalDateTime.now()); Tag t4=new
		 * Tag("fun",LocalDateTime.now(),LocalDateTime.now()); Tag t5=new
		 * Tag("magic",LocalDateTime.now(),LocalDateTime.now());
		 */
		
		SessionFactory f=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Comment.class).addAnnotatedClass(Tag.class).buildSessionFactory();
		Session session=f.getCurrentSession();
		session.beginTransaction();
		Post post=session.get(Post.class,19);
		post.removeTags(session);
		session.getTransaction().commit();
		session.close();
		System.out.println("done");
	}

}
//Post post=new Post("How to lose weight","Summary is to exercise more","here is am gonna write a big content","dheeraj",LocalDateTime.now(),false,LocalDateTime.now(),LocalDateTime.now());		

//Post post=new Post("How to eat","Summary is to exercise more","how to eat content","laxmi",LocalDateTime.now(),true,LocalDateTime.now(),LocalDateTime.now());		