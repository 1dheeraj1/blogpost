package com.mountblue.blog.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mountblue.blog.entity.Comment;
import com.mountblue.blog.entity.Post;
import com.mountblue.blog.entity.Tag;

@Controller
public class MainController 
{
	@RequestMapping("search")
	public String search(@RequestParam("search")String search,Model model)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		
		Query query=session.createQuery("from Post where author like:search or title like :search or content like :search order by "+"author"+" asc");
		query.setParameter("search","%"+search+"%");
		List<Post> posts=query.getResultList();
		model.addAttribute("posts", posts);
		return "postsPage";
	}
	
	@RequestMapping("/")
	public String postsPage(Model model)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		List<Post> posts=session.createQuery("from Post").getResultList();
		model.addAttribute("posts", posts);
		return "postsPage";
	}
	@RequestMapping("/createPost")
	public String createPost(Model model)
	{
		Post post=new Post();
		model.addAttribute("post", post);
		return "createPost";
	}
	@ResponseBody
	@RequestMapping("/addPost")
	public String addPost(@ModelAttribute("post") Post post,HttpServletRequest req)
	{
		String tags=req.getParameter("csv");
		LocalDateTime now=LocalDateTime.now();
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		post.setCreatedAt(now);
		post.setPublishedAt(now);
		post.setUpdatedAt(now);
		
		  String tagValues[]=tags.split(","); 
		  for(String tagValue:tagValues) 
		  {
			  Tag tag=new Tag(tagValue,now,now);
			  session.save(tag);
			  post.addTag(tag); 
			  
		  }
		 
		session.save(post);
		session.getTransaction().commit();
		return "Post added";
	}
	@RequestMapping("/showPostDetails")
	public String addPost(@RequestParam("postId") int postId,Model model)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Post post=session.get(Post.class, postId);
		model.addAttribute("post", post);
		Comment comment=new Comment();
		model.addAttribute("comment", comment);
		return "postDetails";
	}
	
	@RequestMapping("/insertPost")
	public String insertPost(@RequestParam("csv") String csv,@ModelAttribute("post") Post post)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		for(String tagName:csv.split(","))
		{
			Tag tag=new Tag(tagName,LocalDateTime.now(),LocalDateTime.now());
			session.save(tag);
			post.addTag(tag);
		}
		post.setCreatedAt(LocalDateTime.now());
		post.setUpdatedAt(LocalDateTime.now());
		post.setPublishedAt(LocalDateTime.now());
		post.setIsPublished(false);
		session.save(post);
		session.getTransaction().commit();
		return "redirect:/";
	}
	@RequestMapping("/deletePost")
	public String deletePost(@RequestParam("postId") int postId,Model model)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Post post=session.get(Post.class,postId);
		session.delete(post);
		session.getTransaction().commit();
		return "redirect:/";
	}
	@RequestMapping("/addComment")
	public String addComment(@ModelAttribute("comment") Comment comment,@RequestParam("postId") int postId)
	{
		LocalDateTime now=LocalDateTime.now();
		comment.setCreatedAt(now);
		comment.setUpdatedAt(now);
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Post post=session.get(Post.class,postId);
		post.addComment(comment);
		session.save(post);
		session.getTransaction().commit();
		return "redirect:/showPostDetails?postId="+postId;
	}
	@RequestMapping("/deleteComment")
	public String deleteComment(@RequestParam("postId") int postId,@RequestParam("commentId") int commentId)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Comment comment=session.get(Comment.class,commentId);
		session.delete(comment);
		session.getTransaction().commit();
		return "redirect:/showPostDetails?postId="+postId;
	}
	@RequestMapping("/updateCommentForm")
	public String updateCommentForm(@RequestParam("postId") int postId,@RequestParam("commentId") int commentId,Model model)
	{
		System.out.println(postId+"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Comment comment=session.get(Comment.class,commentId);
		Post post=session.get(Post.class,postId);
		System.out.println("post is "+post);
		model.addAttribute("comment", comment);
		model.addAttribute("post", post);
		session.getTransaction().commit();
		return "updateCommentPage";
	}
	@RequestMapping("/updateComment")
	public String updateComment(@ModelAttribute("comment") Comment comment,@RequestParam("pid")int pid)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Comment commentDb=session.get(Comment.class,comment.getId());
		commentDb.setComment(comment.getComment());
		commentDb.setEmail(comment.getEmail());
		commentDb.setName(comment.getName());
		
		commentDb.setUpdatedAt(LocalDateTime.now());
		commentDb.setCreatedAt(LocalDateTime.now());
		session.getTransaction().commit();
		return "redirect:/showPostDetails?postId="+pid;
	}
	@RequestMapping("/updatePostPage")
	public String updatePostPage(@RequestParam("postId")int postId,Model model)
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Post post=session.get(Post.class,postId);
		List<Tag> tags=post.getTags();
		String csv="";
		for(Tag tag:tags)
		{
			csv+=tag.getName()+",";
		}
		model.addAttribute("csv",csv);
		model.addAttribute("post", post);
		return "updatePostPage";
	}
	@RequestMapping("/updatePost")
	public String updatePost(@ModelAttribute("post") Post post,@RequestParam("csv") String csv)
	{
		System.out.println(post.getId()+"nnnnnnnnnnnnnnnnnnnnnn");
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Post.class).addAnnotatedClass(Tag.class).addAnnotatedClass(Comment.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Post postDb=session.get(Post.class,post.getId());
		postDb.setTitle(post.getTitle());
		postDb.setExcerpt(post.getExcerpt());;
		postDb.setContent(post.getContent());
		postDb.setAuthor(post.getAuthor());
		postDb.setPublishedAt(LocalDateTime.now());
		postDb.setIsPublished(false);
		for(Tag tag:postDb.getTags())
		{
			session.delete(tag);
		}
		postDb.setTags(null);
		for(String tagstr:csv.split(","))
		{
			Tag tag=new Tag(tagstr,LocalDateTime.now(),LocalDateTime.now());
			postDb.addTag(tag);
			session.save(tag);
		}
		
		postDb.setUpdatedAt(LocalDateTime.now());
		postDb.setCreatedAt(LocalDateTime.now());
		session.getTransaction().commit();
		return "redirect:/showPostDetails?postId="+post.getId();
	}
	
}
