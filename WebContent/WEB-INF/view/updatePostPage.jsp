<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update Post</title>
</head>
<body>
<form:form action="updatePost" modelAttribute="post">
	<form:hidden path="id"/>
	Title : <form:input path="title"/><br><br>
	Excerpt : <form:input path="excerpt"/><br><br>
	Content : <form:input path="content"/><br><br>
	Author : <form:input path="author"/><br><br>
	Tags : <input type="text" name="csv" value="${csv}">
	<input type="submit" value="Update"><br><br>
</form:form>
</body>
</html>