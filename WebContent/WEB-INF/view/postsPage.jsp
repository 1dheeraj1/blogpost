<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=New+Tegomin&display=swap"
	rel="stylesheet">
<meta charset="UTF-8">
<title>Blog Posts</title>
<style>
* {
	margin: 0px;
	padding: 0px;
}

body {
	font-family: 'New Tegomin', serif;
	color: white;
	background-color: #111;
}

.page {
	width: 100%;
	height: 100vh;
}

.title {
	font-size: 20px;
	position: relative;
	bottom: 10px;
}

.post-container {
	border-radius: 10px;
	border-color: white;
	border-style: solid;
	border-width: 2px;
	text-align: center;
	position: relative;
	height: 3cm;
	margin-bottom: 10px;
}

.author {
	display: flex;
	float: right;
	position: relative;
	bottom: 20px;
	right: 100px;
}
a{
text-decoration: none;
color: red;
}
.updated {
	font-size: 15px;
	color: grey;
}

.read {
	display: flex;
	float: right;
}

.date {
	position: absolute;
	top: 5px;
	left: 5px;
}
</style>
</head>
<body>
	<h2 align="center">
		<a href="createPost">Create a new post</a>
	</h2>
	<form action="search"><input style="border-radius: 5px;" type="text" name="search"><input style="border-radius: 5px;" type="submit" value="Search"></form>
	
	

	<div class="page">
		<c:forEach var="post" items="${posts }">
			<c:url var="readMoreLink" value="/showPostDetails">	
				<c:param name="postId" value="${post.id }"></c:param>
			</c:url>
			<div class="post-container">
				<div class="title-author">
					<div class="date">
						<h5>${post.updatedAt }</h5>
					</div>
					<div class="title">
						<h2 align="center">${post.title}</h2>
					</div>
					<div class="author">By: ${post.author }</div>
				</div>
				<div class="excerpt">
					<h4>${post.excerpt }</h4>
				</div>
				<div class="updated-readmore">

					<div class="read">
						<a href="${readMoreLink}">read more...</a>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>

</body>
</html>