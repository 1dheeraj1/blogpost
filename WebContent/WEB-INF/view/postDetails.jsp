<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
.commentbox{
border: 2px solid black;
margin-bottom:10px;
}

</style>
<title>Post details.</title>
</head>
<body>
	<a href="./">go to home</a>
	<c:forEach var="tag" items="${post.tags }">
	</c:forEach>
	title : ${ post.title}
	<br> author : ${ post.author}
	<br> excerpt : ${ post.excerpt}
	<br> content : ${ post.content}
	<br> pubished At : ${ post.publishedAt}
	<br> is it published? : ${ post.isPublished}
	<br> created at : ${ post.createdAt}
	<br> updated at : ${ post.updatedAt}
	<br> Tags : <c:forEach var="tag" items="${post.tags }">${tag.name},</c:forEach>
	<br>

	<c:url var="deletePostLink" value="/deletePost">
		<c:param name="postId" value="${post.id }"></c:param>
	</c:url>
	<c:url var="updatePostLink" value="/updatePostPage">
		<c:param name="postId" value="${post.id }"></c:param>
	</c:url>

	<br>
	<br>
	<br>
	<a href="${deletePostLink }"> Delete this post</a>
	
	<br>
	<br>
	<a href="${updatePostLink }"> Update this post</a>
	<br> Comment Section:

	<br>
	<br>
	<br>
	<c:forEach var="comment" items="${post.comments }">
	<c:url var="deleteCommentLink" value="/deleteComment">
		<c:param name="commentId" value="${comment.id }"></c:param>
		<c:param name="postId" value="${post.id }"></c:param>
	</c:url>
	<c:url var="updateCommentLink" value="/updateCommentForm">
		<c:param name="commentId" value="${comment.id }"></c:param>
		<c:param name="postId" value="${post.id }"></c:param>
	</c:url>
	<div class="commentbox">
		<div class="details">${comment.name }  at ${comment.updatedAt }</div>
		<div class="comment">${ comment.comment}</div>
		<div class="action">
			<div class="update"><a href="${updateCommentLink }">Update</a></div>
			<div class="delete"><a href="${deleteCommentLink }">Delete</a></div>
		</div>
	</div>
	</c:forEach>
	
	<br>
		<br>
		<br>
		Write your comment
		<br>
		<br>
		<br>
	<form:form action="addComment" modelAttribute="comment">
 	Name : <form:input path="name" />
		<br>
		<br>
		<br>
 	E-Mail : <form:input path="email" />
		<br>
		<br>
		<br> 	
 	Comment : <form:input path="comment" />
		<br>
		<br>
		<br>

		<input type="hidden" value="${post.id }" name="postId">
		<input type="submit" value="OK">
	</form:form>
</body>
</html>