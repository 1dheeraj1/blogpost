<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add a new Post</title>
</head>
<body>
<a href="./">go to home</a>
	<h1 align="center">This is the New Post form</h1>
	<br><br><br>
	<form:form action="insertPost" modelAttribute="post" align="center">
		Title : <form:input path="title"/>
		Excerpt : <form:input path="excerpt"/>
		Content : <form:input path="content"/>
		Author : <form:input path="author"/>
	<input type="text" name="csv" placeholder="Enter tags seperated by comma"> 
	<input type="submit" value="Post">
	</form:form> 
</body>
</html>