<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update Comment</title>
</head>
<body>
${post.id}
<form:form action="updateComment" modelAttribute="comment">
	<form:hidden path="id"/>
	<input type="hidden" name="pid" value="${post.id}">
	Name : <form:input path="name"/><br><br>
	E-Mail : <form:input path="email"/><br><br>
	Comment : <form:input path="comment"/><br><br>
	<input type="submit" value="Update"><br><br>
</form:form>
</body>
</html>